
import 'package:application2/models/question.dart';

List<Question> getQuestions(){

  List<Question> questions =  [];
  Question questionModel = Question(question: '', isCorrect: false);

  //1
  questionModel.question ="Le drapeau français est bleu, blanc et rouge ?";
  questionModel.isCorrect =true;
  questions.add(questionModel);
  questionModel = Question(question: '', isCorrect: false);

  //2
  questionModel.question ="Bruxelles n’est pas en France ?";
  questionModel.isCorrect =true;
  questions.add(questionModel);
  questionModel = Question(question: '', isCorrect: false);

  //3
  questionModel.question ="La capitale de la France est Berlin ?";
  questionModel.isCorrect =false;
  questions.add(questionModel);

  return questions;

}