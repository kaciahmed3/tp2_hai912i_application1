import 'package:flutter/material.dart';
import 'package:application2/views/homepage.dart';
import 'package:provider/provider.dart';
import 'Providers/question_provider.dart';
import 'data/data.dart';


void main() => runApp(  ChangeNotifierProvider(create: (context)=>QuestionProvider(getQuestions(),0,0),
  child:MyApp(),));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: HomePage(),
    );
  }
}