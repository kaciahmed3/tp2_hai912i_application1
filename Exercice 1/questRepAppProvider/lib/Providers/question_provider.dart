import 'package:application2/views/result.dart';
import 'package:flutter/material.dart';
import 'package:application2/models/question.dart';

class QuestionProvider extends ChangeNotifier {

  List<Question> _questions ;
  int _index ;
  int _score;

  QuestionProvider(this._questions, this._index, this._score);

  void nextQuestion(BuildContext context) {
    if (index < questions.length - 1) {
      index++;
    } else {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => Result(
                  score: _score
              )));
    }
    notifyListeners();
  }
  void checkAnswer(bool userChoice, BuildContext context){
    if (questions[index].isCorrect==userChoice) {
        _score++ ;
        nextQuestion(context);
    } else {
        _score--;
        nextQuestion(context);
    }
    notifyListeners();
  }
  void restart(){
      index =0;
      score=0;
      notifyListeners();
  }
// getters && setters

  int get score => _score;

  set score(int value) {
    _score = value;
  }

  int get index => _index;

  set index(int value) {
    _index = value;
  }

  List<Question> get questions => _questions;

  set questions(List<Question> value) {
    _questions = value;
  }
}