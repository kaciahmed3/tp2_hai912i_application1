import 'package:application2/data/data.dart';
import 'package:flutter/material.dart';
import 'package:application2/views/homepage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'cubits/questions_quiz_cubit.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => QuestionsQuizCubit(getQuestions(),0,0),
        child: BlocBuilder<QuestionsQuizCubit, QuestionsQuizState >(
        builder: (_, theme) {
          return MaterialApp(
            debugShowCheckedModeBanner: true,
            theme: ThemeData(
              primarySwatch: Colors.blueGrey,
            ),
            home: HomePage(),
          );
        }
        )
    );
  }
}